import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ListFilmsComponent } from './components/list-films/list-films.component';
import { DetailFilmComponent } from './components/detail-film/detail-film.component';
import { CharacterStarshipsComponent } from './components/character-starships/character-starships.component';

import { StarWarsService } from './services/star-wars.service';

export const routes: Routes = [
    { path: '', redirectTo: 'list-films', pathMatch: 'full' },
    { path: 'list-films', component: ListFilmsComponent},
    { path: 'detail-film/:filmId', component: DetailFilmComponent},
    { path: 'character-starships/:characterId/:filmId', component: CharacterStarshipsComponent}
];

@NgModule({
    declarations: [
        AppComponent,
        ListFilmsComponent,
        DetailFilmComponent,
        CharacterStarshipsComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        RouterModule.forRoot(routes),
    ],
    providers: [StarWarsService],
    bootstrap: [AppComponent]
})
export class AppModule { }
