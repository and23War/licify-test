import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StarWarsService } from '../../services/star-wars.service';

@Component({
  selector: 'app-detail-film',
  templateUrl: './detail-film.component.html',
  styleUrls: ['./detail-film.component.sass']
})
export class DetailFilmComponent implements OnInit {
    loaded = false;
    filmId: string;
    film: any = {};
    characters: Array<any> = [];

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private starWarsService: StarWarsService
        ) { }

    ngOnInit() {
        this.filmId = this.activatedRoute.snapshot.params['filmId'];
        this.getFilmByIdFromAPI(this.filmId)
            .then(() => {
                return this.prepareCharacters();
            }).then(() => {
                this.loaded = true;
            })
            .catch(error => {
                this.loaded = true;
                console.log(error);
            });
    }

    private getFilmByIdFromAPI(filmId): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.starWarsService.getFilmById(filmId)
                .then(response => {
                    this.film = response;
                    resolve(true);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    private prepareCharacters(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const promises = [];
            this.film.characters.forEach(c => {
                const pieces = c.split('/');
                const characterId = pieces[pieces.length - 2];
                promises.push(this.getCharacterFromAPI(characterId));
            });

            Promise.all(promises)
                .then(response => {
                    this.characters = response;
                    resolve(true);
                })
                .catch(error => {
                    reject(error);
                });
        });
  }

    private getCharacterFromAPI(characterId): Promise<any> {
        return new  Promise<any>((resolve, reject) => {
            this.starWarsService.getCharacterById(characterId)
                .then(response => {
                    response.characterId = characterId;
                    resolve(response);
                })
                .catch(error => {
                    alert('Ups! Lo sentimos ha ocurrido un error, intentelo de nuevo más tarde.');
                    reject(error);
                });
        });
  }

    goToCharacterStarships(characterId) {
        this.router.navigate(['character-starships', characterId, this.filmId]);
    }

    goToListFilms() {
        this.router.navigate(['list-films']);
    }

}
