import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { StarWarsService } from '../../services/star-wars.service';

@Component({
  selector: 'app-list-films',
  templateUrl: './list-films.component.html',
  styleUrls: ['./list-films.component.sass']
})
export class ListFilmsComponent implements OnInit {
    films: Array<any> = [];
    loaded = false;

    constructor(
        private starWarsService: StarWarsService,
        private router: Router
    ) { }

    ngOnInit() {
        this.getAllFilmsFromAPI()
            .then(() => {
                this.loaded = true;
            })
            .catch(error => {
                console.log(error);
            });
    }

    private getAllFilmsFromAPI(): Promise<any> {
        return new Promise<any> ((resolve, reject) => {
            this.starWarsService.getFilms()
                .then(response => {
                    this.films = response.results;
                    resolve(true);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    goToDetailMovie(film) {
        const pieces = film.url.split('/');
        const filmId = pieces[pieces.length - 2];
        this.router.navigate(['detail-film', filmId]);
    }

}
