import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StarWarsService } from '../../services/star-wars.service';

@Component({
  selector: 'app-character-starships',
  templateUrl: './character-starships.component.html',
  styleUrls: ['./character-starships.component.sass']
})
export class CharacterStarshipsComponent implements OnInit {
    loaded = false;
    characterId: string;
    filmId: string;
    character: any = {};
    starships: Array<any> = [];

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private starWarsService: StarWarsService
    ) { }

    ngOnInit() {
        this.characterId = this.activatedRoute.snapshot.params['characterId'];
        this.filmId = this.activatedRoute.snapshot.params['filmId'];
        this.getCharacterFromAPI(this.characterId)
            .then(() => {
                return this.prepareStarships();
            })
            .then(() => {
                this.loaded = true;
            }).catch(error => {
                this.loaded = true;
                console.log(error);
            });
    }

    private getCharacterFromAPI(characterId): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.starWarsService.getCharacterById(characterId)
                .then(response => {
                    this.character = response;
                    resolve(true);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    private prepareStarships(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const promises = [];
            this.character.starships.forEach(c => {
                const starshipId = this.getIdByURL(c);
                promises.push(this.getStarshipFromAPI(starshipId)
                        .then(starship => {
                            starship.dataFilms = [];
                            return this.prepareFilmsByStarship(starship);
                        }));
            });

            Promise.all(promises)
                .then(response => {
                    this.starships = response;
                    resolve(true);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    private prepareFilmsByStarship(starship): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const promises = [];
            starship.films.forEach(f => {
                const filmId = this.getIdByURL(f);
                promises.push(
                    this.getFilmByIdFromAPI(filmId)
                        .then(film => {
                            starship.dataFilms.push(film);
                        }));
            });

            Promise.all(promises)
                .then(() => {
                    resolve(starship);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    private getIdByURL(url) {
        const pieces = url.split('/');
        return pieces[pieces.length - 2];
    }

    private getStarshipFromAPI(starshipId): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.starWarsService.getStarshipById(starshipId)
                .then(response => {
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    private getFilmByIdFromAPI(filmId): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.starWarsService.getFilmById(filmId)
                .then(response => {
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    goToDetailMovie(filmId) {
        this.router.navigate(['detail-film', filmId]);
    }

}
