import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { environment } from '../../environments/environment';

@Injectable()
export class StarWarsService {

  headers = new Headers({ 'Content-Type': 'application/json' });
  options = new RequestOptions({ headers: this.headers });
  constructor(private http: Http) { }

    getFilms() {
        return this.http.get(environment.starWarsAPI + 'films/', this.options)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getFilmById(filmId) {
        return this.http.get(environment.starWarsAPI + 'films/' + filmId, this.options)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }


    getCharacterById(characterId) {
        return this.http.get(environment.starWarsAPI + 'people/' + characterId, this.options)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getStarshipById(starship) {
    return this.http.get(environment.starWarsAPI + 'starships/' + starship, this.options)
        .toPromise()
        .then(this.extractData)
        .catch(this.handleError);
    }

    private extractData(res: Response) {
        const response = res.json();
        return response || { };
    }

    private handleError(error: any) {
        console.error('Ha ocurrido un error', error);
        return Promise.reject(error.message || error);
    }

}
